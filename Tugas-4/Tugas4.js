//Soal 1
//Looping Pertama
console.log("Jawaban No.1")
console.log("LOOPING PERTAMA");
var deret = 2;
while(deret <= 20) { 
  console.log(deret + "- I love coding"); 
  deret+=2; 
}
//Looping Kedua
console.log("LOOPING KEDUA");
var decderet = 20;
while(decderet > 0) { 
  console.log(decderet + "- I will become a frontend developer"); 
  decderet-=2; 
}

//Soal 2
console.log("\nJawaban No.2")
for(var angka = 1; angka <= 20; angka++){
    if((angka%2)==1){
      if((angka%3)==0){
        console.log(angka + "- I Love Coding");
      }else{
      console.log(angka + "- Santai");
      }
    }
    else if ((angka%2)===0) {
      console.log(angka + "- Berkualitas");
    }
  }

//soal 3
var n = '';
for(var i = 0; i < 7; i++){
   for(var j = 0; j <= i; j++){
       n += '#';
   }
   n +='\n';
}
console.log("\nJawaban No.3"+"\n"+n);

//Soal 4
var kalimat="Saya sangat senang belajar javascript";
var name = kalimat.split(" ");
console.log("\nJawaban No.4");
console.log(name);

//Soal 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
var sorting = daftarBuah.sort();
var slug = sorting.join("\n");
console.log("\nJawaban No.5"+"\n" + slug);