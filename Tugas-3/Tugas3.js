//soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

var pisah = kataKedua.substr(0,1);
var kapitalDua = kataKedua.substring(1);
var kapitalSatu = pisah.toUpperCase();
var upper1 = kapitalSatu.concat(kapitalDua);
var upper = kataKeempat.toUpperCase();
console.log("\nJawaban No.1");
console.log(kataPertama.concat(" "+upper1+" "+kataKetiga+" "+upper));

//soal 2
var angkaPertama = "1";
var angkaKedua = "2";
var angkaKetiga = "4";
var angkaKeempat = "5";

var integer1 = parseInt(angkaPertama);
var integer2 = parseInt(angkaKedua);
var integer3 = parseInt(angkaKetiga);
var integer4 = parseInt(angkaKeempat);
var hasil = integer1 + integer2 + integer3 + integer4;

console.log("\nJawaban No.2");
console.log(hasil);

//soal 3
var kalimat = 'wah javascript itu kereen sekali';
var kalimatPertama = kalimat.substring(0, 3);
var kalimatKedua = kalimat.substring(4, 15);
var kalimatKetiga = kalimat.substring(15, 18);
var kalimatKeempat = kalimat.substring(19, 26);
var kalimatKelima = kalimat.substring(26, 32);

console.log("\nJawaban No.3");
console.log('Kata Pertama: ' + kalimatPertama);
console.log('Kata Kedua: ' + kalimatKedua);
console.log('Kata Ketiga: ' + kalimatKetiga);
console.log('Kata Keempat: ' + kalimatKeempat);
console.log('Kata Kelima: ' + kalimatKelima);

//soal 4
console.log("\nJawaban No.4");

var nilai = 95;
if(nilai >= 80){
    console.log("indeksnya A");
}else if(nilai >= 70 && nilai < 80){
    console.log("Indeksnya B");
}else if(nilai >=60 && nilai < 70){
    console.log("Indeksnya C");
}else if(nilai >=50 && nilai < 60){
    console.log("Indeksnya D");
}else{
    console.log("Indeksnya E");
}

//soal 5
var tanggal = 13;
var bulan = 04;
var tahun = 2000;

switch(bulan){
    case 01:{
        var bulan = "Januari"; break;
    }case 02:{
        var bulan = "Februari"; break;
    }case 03:{
        var bulan = "Maret"; break;
    }case 04:{
        var bulan = "April"; break;
    }case 05:{
        var bulan = "Mei"; break;
    }case 06:{
        var bulan = "Juni"; break;
    }case 07:{
        var bulan = "Juli"; break;
    }case 08:{
        var bulan = "Agustus"; break;
    }case 09:{
        var bulan = "September"; break;
    }case 10:{
        var bulan = "Oktober"; break;
    }case 11:{
        var bulan = "November"; break;
    }case 12:{
        var bulan = "Desember"; break;
    }
    return;
}

stringTanggal = tanggal.toString();
stringTahun = tahun.toString();
console.log("\nJawaban No.5");
console.log(stringTanggal.concat(" "+bulan+" "+stringTahun));



