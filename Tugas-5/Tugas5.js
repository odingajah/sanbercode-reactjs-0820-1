//Soal 1
function halo(hello = "Halo Sanbers!") {
    return hello;
}
console.log("Jawaban No.1\n" + halo()); 

//Soal 2
function kalikan(num1, num2){
    return num1 * num2;
}
var num1 = 12;
var num2 = 4;

var hasilKali = kalikan (num1, num2);
console.log("\nJawaban No.2\n" + hasilKali);

//Soal 3
function introduce(name, age, address, hobby){
    return "Nama saya "+name+", Umur saya "+age+" tahun, alamat saya di "+address+", dan saya punya hobby yaitu "+hobby+"!";
}

var name = "John";
var age = 30;
var address = "Jalan belum jadi";
var hobby = "Gaming";
 
var perkenalan = introduce(name, age, address, hobby);
console.log("\nJawaban No.3\n" + perkenalan);

//Soal 4
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992];

var arrayDaftarPesertaBaru = {
    namanya : "Asep",
    jenisKelaminya: "laki-laki",
    Hobbinya: "baca buku",
    tahunLahirnya: 1992
};

console.log("\nJawaban No.4\nNama: " + arrayDaftarPesertaBaru.namanya);
console.log ("Jenis kelamin: " + arrayDaftarPesertaBaru.jenisKelaminya);
console.log ("Hobi: " + arrayDaftarPesertaBaru.Hobbinya);
console.log ("Tahun lahir: " + arrayDaftarPesertaBaru.tahunLahirnya);

//Soal 5
var daftarBuah =[{"1. nama": "strawberry", "warna":  "merah", "ada bijinya": "tidak", "harga": 9000 },
                {"2. nama": "jeruk", "warna": "oranye", "ada bijinya": "ada", "harga": 8000},
                {"3. nama": "semangka", "warna": "hijau & merah", "ada bijinya": "ada", "harga": 10000},
                {"4. nama": "pisang", "warna": "kuning", "ada bijinya": "tidak", "harga": 5000}];
                
console.log("\nJawaban No.5");
console.log(daftarBuah[0]);

//Soal 6
var dataFilm = [];
var counter = 0;
var temporary = {};
function addDatafilm(judul, genre, durasi, tahun){
     temporary.Nama = judul;
     temporary.Genre = genre;
     temporary.Durasi = durasi;
     temporary.Tahun = tahun;
     dataFilm[counter] = temporary;   
     counter++;
};
 addDatafilm("Orphan","Horror","2 jam 3 menit", 2009);
 console.log("\nJawaban No.6");
 console.log(dataFilm);