//Soal No.1
const luasLingkaran = (jari) => {
    let pi = jari % 7 === 0 ? 22/7 : 3.14;
    return pi * jari * jari
}
  
const kelilingLingkaran = (jari) => {
    let pi = jari % 7 === 0 ? 22/7 : 3.14;
    return 2 * pi * jari
}
  
  console.log("Jawaban No.1\nLuas Lingkaran dengan jari2 = 10 adalah?"+"\nJawab: "+luasLingkaran(10))
  console.log("Keliling Lingkaran dengan jari2 = 7 adalah?"+"\nJawab: "+kelilingLingkaran(7))

//Soal No.2
let kalimat =""

const addKalimat = (kalimat1,kalimat2,kalimat3,kalimat4,kalimat5) =>{
    kalimat = `${kalimat1} ${kalimat2} ${kalimat3} ${kalimat4} ${kalimat5}`
}

addKalimat ("saya","adalah","seorang","frontend","developer")

console.log("\nJawaban No.2\n"+kalimat)

//Soal No.3
console.log("\nJawaban No.3")
const newFunction = function literal(firstName, lastName){
    return {
      firstName, lastName,
      fullName: function(){
        console.log(`${firstName} ${lastName}`)
        return 
      }
    }
  }
   
//Driver Code 
newFunction("William", "Imoh").fullName()

//Soal No.4
console.log("\nJawaban No.4")

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

// before (with es5)
// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const destination = newObject.destination;
// const occupation = newObject.occupation;
// const spell = newObject.spell;

//after (with es6)
const {firstName, lastName, destination, occupation, spell} = newObject
  
// Driver code
console.log(firstName, lastName, destination, occupation)

//Soal No.5
console.log("\nJawaban No.5")

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

//before (with es5)
//const combined = west.concat(east)

//after (with es6)
const combined = [...west,...east]

//Driver Code
console.log(combined)